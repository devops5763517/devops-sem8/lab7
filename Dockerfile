
ARG SYSTEM_IMAGE=azul/zulu-openjdk-alpine:17-latest
ARG DEPENDENCY_IMAGE=system
ARG PREBUILD_APP_IMAGE=build

FROM ${SYSTEM_IMAGE} AS system
WORKDIR /app
COPY ./ ./

FROM system AS build

RUN chmod +x gradlew && ./gradlew clean build -x test --no-daemon

RUN apk add binutils

RUN jlink \
    --verbose \
    --add-modules \
        java.base,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --compress 2 \
    --strip-debug \
    --no-header-files \
    --no-man-pages \
    --output /opt/jre

FROM alpine as project
COPY --from=build /opt/jre /opt/jre

WORKDIR /app
COPY --from=build /app/build/libs/*.jar ./app.jar

ENTRYPOINT [ "/opt/jre/bin/java" ]
CMD [ "-jar", "app.jar"]

